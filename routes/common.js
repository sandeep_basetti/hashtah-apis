'use strict';

const express = require('express');

const stateManager = require('../modules/controller');
const routeUtil =  require('../utils/routeUtil');

module.exports = function (app) {
    
    app.get('/states', getStates);
    app.get('/districts', getDistrict);
    app.get('/cities', getCity);

    app.post('/states', addStates);


    function getStates(req, res) {
        return stateManager.getStates(req.query.q).then((states) => {
            return routeUtil(res, states, 200);
        }).catch((err) => {
            return routeUtil(res, `error when fetching states: ${err}`, 500);
        });
    }

    function getDistrict(req, res) {
        return stateManager.getDistrict(req.query.q).then((states) => {
            return routeUtil(res, states, 200);
        }).catch((err) => {
            return routeUtil(res, `error when fetching states: ${err}`, 500);
        });
    }

    function getCity(req, res) {
        return stateManager.getCity(req.query.q).then((states) => {
            return routeUtil(res, states, 200);
        }).catch((err) => {
            return routeUtil(res, `error when fetching states: ${err}`, 500);
        });
    }

    function addStates(req, res) {
        return stateManager.addStates(req.body).then((states) => {
            return routeUtil(res, states, 200);
        }).catch((err) => {
            return routeUtil(res, `error when adding states: ${err}`, 500);
        });
    }
}
