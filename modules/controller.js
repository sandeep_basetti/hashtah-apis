'user strict';
const State = require('../models/state');
const District = require('../models/district');
const City = require('../models/city');

const controller = {};
// .find({stateCode: { $in: states }})
controller.getStates = (input) => {
    let stateName = new RegExp(input, "i");
    return State.find({ state: stateName }, { state: 1, stateCode: 1, _id: 0 }).then((states) => {
        if (!states || !Array.isArray(states) || states.length == 0) {
            return 'No states found';
        }
        else {
            let stateCodes = states.map(ele => { return ele.stateCode })
            return District.find({ stateCode: { $in: stateCodes } }, { _id: 0 }).then((districts) => {
                if (!districts || !Array.isArray(districts) || districts.length == 0) {
                    return 'No districts found';
                }
                districts.map((ele, i) => {
                    districts[i]["_doc"]["state"] = states.find((item) => item.stateCode === districts[i].stateCode).state
                })
                return districts;
            });
        }
    });
}

controller.getDistrict = (input) => {
    let districtName = new RegExp(input, "i");
    return District.find({ district: districtName }, { _id: 0 }).then((districts) => {
        if (!districts || !Array.isArray(districts) || districts.length == 0) {
            return 'No districts found';
        }
        else {
            let districCodes = districts.map(ele => { return {districtCode : ele.districtCode, stateCode: ele.stateCode} })
            return City.find({ $and: districCodes } , { _id: 0 }).then((cities) => {
                if (!cities || !Array.isArray(cities) || cities.length == 0) {
                    return 'No cities found';
                }
                cities.map((ele, i) => {
                    cities[i]["_doc"]["district"] = districts.find((item) => item.districtCode === cities[i].districtCode).district;
                    cities[i]["_doc"]["stateCode"] = districts.find((item) => item.districtCode === cities[i].districtCode).stateCode;
                })
                return cities;
            });
        }
    });
}

controller.getCity = (input) => {
    let cityName = new RegExp(input, "i");
    return City.find({ city: cityName }, { _id: 0 }).then((cities) => {
        if (!cities || !Array.isArray(cities) || cities.length == 0) {
            return 'No cities found';
        }
        else {
            // return cities;
            let districCodes = cities.map(ele => { return {districtCode : ele.districtCode, stateCode: ele.stateCode} })
            return District.find({ $and: districCodes }, { _id: 0 }).then((districts) => {
                if (!districts || !Array.isArray(districts) || districts.length == 0) {
                    return 'No districts found';
                }
                else {
                    cities.map((ele, i) => {
                        cities[i]["_doc"]["district"] = districts.find((item) => item.districtCode === cities[i].districtCode).district;
                    })
                    let stateCodes = cities.map(ele => { return ele.stateCode })

                    return State.find({ stateCode: { $in: stateCodes } }, { _id: 0 }).then((states) => {
                        if (!states || !Array.isArray(states) || states.length == 0) {
                            return 'No states found';
                        }
                        else {
                            cities.map((ele, i) => {
                                cities[i]["_doc"]["state"] = states.find((item) => item.stateCode === cities[i].stateCode).state
                            })
                            return cities;
                        }
                    });
                }
            });

        }
    });
}

controller.addStates = (stateData) => {
    let state = new State();
    state.state = stateData.state;
    state.stateCode = stateData.stateCode;

    return state.save().then(() => {
        return "State saved successfully"
    });
}


module.exports = controller;
