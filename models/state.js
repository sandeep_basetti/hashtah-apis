const mongoose = require('mongoose');

require('../db');

let stateSchema = mongoose.Schema({
    state: { type: String},
    stateCode: { type: Number, unique: true }
});

module.exports = mongoose.model('state', stateSchema);
