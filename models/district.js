const mongoose = require('mongoose');

require('../db');

let districtSchema = mongoose.Schema({
    district: { type: String},
	districtCode:{ type: Number},
    stateCode:{ type: Number}
});

module.exports = mongoose.model('district', districtSchema);
