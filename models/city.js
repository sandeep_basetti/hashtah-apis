const mongoose = require('mongoose');

require('../db');

let citySchema = mongoose.Schema({
    city: { type: String},
    urbanStatus :  { type: String},
    stateCode:{ type: Number},
	districtCode:{ type: Number}
});

module.exports = mongoose.model('city', citySchema);
