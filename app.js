'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const http = require('http');
const config = require('./config');
const routeUtil =  require('./utils/routeUtil');
const app = express();
const server = http.createServer(app);

const logs = require("./utils/logs");
logs(app);

app.use(cors());


let options = {
  inflate: true,
  limit: '100kb',
  type: 'application/json'
};

app.use(morgan('dev'));

app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.raw(options));

app.use(express.static('./docs'));
app.use(function (req, res, next) {
  if (!req.query.q) {
    routeUtil(res, `User Input Is Missing`, 400);
  }
  else {
    next();
  }
});


require('./routes/common')(app);

server.listen(config.port, () => {
  console.log(`Express server started on ${config.server}, listening on port ${config.port} in ${app.get('env')} mode`);
});


app.get('*', function (req, res, next) {
  let err = new Error(`${req.ip} tried to reach ${req.originalUrl}`);
  err.statusCode = 404;
  next(err);
});
