'use strict'

const config = {};

config.port = process.env.PORT || 8080;
config.server = process.env.SERVER || 'localhost';
config.db = {
  'url': process.env.DB_URL || 'mongodb://localhost:27017',
  'name': process.env.DB_NAME || 'hashtag',
  'user': process.env.DB_USER || '',
  'password': encodeURIComponent(process.env.DB_PASSWORD) || '',
}

config.secret = process.env.SECRET || 'mock#secret'

module.exports = config;